const express = require('express');
const moment = require('moment');
const fileUse = require('./fileWork');
const router = express.Router();

router.get('/', (req, res) => {
    const messages = fileUse.getMessages();
    res.send(messages);
});

router.get('/:id', (req, res) => {
    res.send('get with id = ' + req.params.id)
});

router.post('/', (req, res) => {
    const mes = req.body;
    mes.datetime = moment().format();
    fileUse.addMessage(req.body);
    res.send(mes);
});

module.exports = router;