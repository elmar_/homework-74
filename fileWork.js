const fs = require('fs');
const mkdirp = require('mkdirp');

const filePath = './messages/';

let messagesToGet = [];

module.exports = {
    init() {
        console.log('init');
    },
    getMessages() {
        try {
            messagesToGet = [];
            const files = fs.readdirSync(filePath, null);
            let i = 0;
            files.reverse().forEach(file => {
                if (i < 5) {
                    const fileData = fs.readFileSync(filePath + file);
                    messagesToGet.push(JSON.parse(fileData));
                }
                i++;
            });
            return (messagesToGet);
        } catch (e) {
            return messagesToGet;
        }
    },
    addMessage(message) {
        try {
            fs.writeFileSync(filePath + message.datetime + '.json', JSON.stringify(message, null, 2));
        } catch (e) {
            mkdirp(filePath);
            //fs.writeFileSync(filePath + message.datetime + '.json', JSON.stringify(message, null, 2));
            console.log('повторите');
        }
    }
};