const express = require('express');
const messages = require('./messages');
const fileUse = require('./fileWork');

fileUse.init();

const app = express();
app.use(express.json());

const port = 8000;

app.use('/messages', messages);


app.listen(port, () => {
    console.log("We are on port " + port);
});